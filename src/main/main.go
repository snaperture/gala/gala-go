package main

import (
	"flag"
	"gala/src/generator"
	"gala/src/indexer"
	"log"
)

func main() {
	// Command line options
	mediaRoot := flag.String("root", "media", "where to start looking for albums and media")
	outputRoot := flag.String("output", "site", "where to output the static site")
	verbose := flag.Bool("v", false, "whether to log more details while running")
	title := flag.String("title", "Gallery", "title for the generated site")
	thumbnailWidth := flag.Int("thumb-width", 150, "width for generated thumbnails")
	thumbnailHeight := flag.Int("thumb-height", 100, "height for generated thumbnails")
	template := flag.String("template", "template", "location of the template to use")
	flag.Parse()

	// Index media root to find albums and media
	index := indexer.IndexMedia(*mediaRoot)
	log.Println("Indexing completed")

	if *verbose {
		indexer.PrintIndex(&index)
	}

	// Generate thumbnails
	rootAlbum := ProcessAlbum(&index)

	// Output HTML site
	generator.GenerateSite(&generator.Gallery{
		Title:           *title,
		ThumbnailWidth:  *thumbnailWidth,
		ThumbnailHeight: *thumbnailHeight,
		Template:        *template,
	}, &rootAlbum, *outputRoot)
	log.Println("Site generation completed")
}
