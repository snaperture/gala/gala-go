package main

import (
	"gala/src/generator"
	"gala/src/indexer"
	"path/filepath"
	"sort"
	"strings"
)

func ProcessAlbum(album *indexer.IndexedAlbum) generator.Album {
	var generateAlbum func(album *indexer.IndexedAlbum) generator.Album
	generateAlbum = func(album *indexer.IndexedAlbum) generator.Album {
		subAlbums := make([]generator.Album, 0, len(album.Albums))
		mediaList := make([]generator.Media, 0, len(album.Media))

		for _, subAlbum := range album.Albums {
			subAlbums = append(subAlbums, generateAlbum(&subAlbum))
		}

		for _, media := range album.Media {
			mediaList = append(mediaList, processMedia(&media))
		}

		sort.Slice(subAlbums, func(i, j int) bool {
			return subAlbums[i].Name < subAlbums[j].Name
		})

		sort.Slice(mediaList, func(i, j int) bool {
			return mediaList[i].Name < mediaList[j].Name
		})

		return generator.Album{
			Name:   album.Entity.Name,
			Path:   album.Entity.Path,
			Albums: subAlbums,
			Media:  mediaList,
		}
	}

	return generateAlbum(album)
}

func processMedia(media *indexer.IndexedMedia) generator.Media {
	return generator.Media{
		Name: media.Entity.Name,
		Path: strings.TrimSuffix(media.Entity.Path, filepath.Ext(media.Entity.Path)),
	}
}
