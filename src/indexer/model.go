package indexer

import "fmt"

type FSEntity struct {
	Path     string // Path relative to the gallery root
	FilePath string // Location of the file/media or directory/album on disk
	Name     string // Visitor-facing name of the file/media or directory/album
}

type IndexedAlbum struct {
	Entity FSEntity                // Filesystem directory backing this album
	Albums map[string]IndexedAlbum // Sub-albums mapped by Name
	Media  map[string]IndexedMedia // Media items mapped by Name
}

type IndexedMedia struct {
	Entity FSEntity // Filesystem file backing this media
}

func (entity *FSEntity) String() string {
	return fmt.Sprintf("'%s' (%s) from %s", entity.Name, entity.Path, entity.FilePath)
}

func (album *IndexedAlbum) String() string {
	return "Album " + album.Entity.String()
}

func (media *IndexedMedia) String() string {
	return "Media " + media.Entity.String()
}
