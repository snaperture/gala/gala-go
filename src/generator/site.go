package generator

import (
	"github.com/otiai10/copy"
	"html/template"
	"log"
	"os"
	"path"
)

func GenerateSite(gallery *Gallery, rootAlbum *Album, outputRoot string) {
	// Delete old site
	if err := os.RemoveAll(outputRoot); err != nil {
		log.Fatalln("Error emptying output directory:", err)
	}

	// Load templates
	indexTemplate := loadTemplate("index")
	albumTemplate := loadTemplate("album")
	mediaTemplate := loadTemplate("media")

	// Make sure output directory exists
	if _, err := os.Stat(outputRoot); os.IsNotExist(err) {
		if err := os.Mkdir(outputRoot, os.ModePerm.Perm()); err != nil {
			log.Fatalln("Error creating output directory:", err)
		}
	}

	// Generate homepage
	renderTemplate(indexTemplate, path.Join(outputRoot, "index.html"), DataForTemplate(gallery, rootAlbum, 0))

	var generateSite func(album *Album, level int)
	generateSite = func(album *Album, level int) {
		// Generate album page
		if album != rootAlbum {
			renderTemplate(albumTemplate, path.Join(outputRoot, album.Path, "index.html"), DataForTemplate(gallery, album, level))
		}

		// Generate media pages
		for _, media := range album.Media {
			renderTemplate(mediaTemplate, path.Join(outputRoot, media.Path+".html"), DataForTemplate(gallery, media, level))
		}

		// Generate sub-albums
		for _, subAlbum := range album.Albums {
			generateSite(&subAlbum, level+1)
		}
	}

	generateSite(rootAlbum, 0)

	// Copy resources
	if err := copy.Copy(path.Join(gallery.Template, "resources"), path.Join(outputRoot, "resources")); err != nil {
		log.Fatalln("Error copying resources:", err)
	}
}

func loadTemplate(templateFile string) *template.Template {
	templateFile = path.Join("template", templateFile+".gohtml")

	if t, err := template.ParseFiles(templateFile); err != nil {
		log.Fatalln("Error loading template:", err)
		return nil
	} else {
		return t
	}
}

func renderTemplate(template *template.Template, destinationFile string, data TemplateData) {
	if err := os.MkdirAll(path.Dir(destinationFile), os.ModeDir); err != nil {
		log.Fatalln("Error creating output directory:", err)
	}

	if file, err := os.Create(path.Clean(destinationFile)); err != nil {
		log.Fatalln("Error creating output file:", err)
	} else {
		if err := template.Execute(file, data); err != nil {
			log.Fatalln("Error executing template:", err)
		}

		if err := file.Close(); err != nil {
			log.Fatalln("Error closing output file:", err)
		}
	}
}
